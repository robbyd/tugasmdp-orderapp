package stts.edu.orderapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    private ArrayList<Order> orderList;
    private static  RVCClickListener myListener;
    public OrderAdapter(ArrayList<Order> orderList, RVCClickListener rvcl){
        this.orderList = orderList;
        myListener = rvcl;
    }

    @NonNull
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.row_item_order, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAdapter.ViewHolder viewHolder, int i) {
        Order temp = orderList.get(i);
        String topping = "with toppings: ";
        for (int j = 0; j < temp.getToppings().size(); j++) {
            topping += temp.getToppings().get(j) + ", ";
        }
        viewHolder.tvQtyType.setText(temp.getQty() + " " + temp.getType());
        viewHolder.tvToppings.setText(topping);
        viewHolder.tvSubtotal.setText(temp.getSubtotal()+"");
    }

    @Override
    //mengembalikan jumlah data yang mau di load, dari ArrayList ke RecyclerView
    public int getItemCount() {
        return (orderList!=null) ? orderList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvQtyType, tvToppings, tvSubtotal;
        public ViewHolder (@NonNull final View itemView){
            super(itemView);
            tvQtyType = itemView.findViewById(R.id.textView_qty_type);
            tvToppings = itemView.findViewById(R.id.textView_toppings);
            tvSubtotal = itemView.findViewById(R.id.textView_subtotal);
            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    myListener.recyclerViewClick(v, ViewHolder.this.getLayoutPosition());
                }
            });
        }
    }
}
