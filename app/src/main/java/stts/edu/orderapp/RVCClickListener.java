package stts.edu.orderapp;

import android.view.View;

public interface RVCClickListener {
    public void recyclerViewClick(View v, int posisi);
}
